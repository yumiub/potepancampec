require 'rails_helper'

RSpec.feature "Potepan::Categories", type: :feature do
  let(:taxonomy) { create(:taxonomy, taxons: [taxon]) }
  let(:taxon) { create(:taxon, products: [product]) }
  let(:other_taxon) { create(:taxon) }
  let(:product) { create(:product) }
  let!(:other_product) { create(:product, name: "other bag", taxons: [other_taxon]) }

  before do
    visit potepan_category_path(id: taxon.id)
  end

  scenario 'カテゴリーページが正しく表示されていること' do
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_selector('img[alt="products-img"]')
  end

  scenario 'サイドバーが正しく表示されていること' do
    within ".side-nav" do
      expect(page).to have_content taxonomy.name
    end
  end

  scenario 'カテゴリーページから商品詳細ページへ移動すること' do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
  end

  scenario 'タイトルがカテゴリ名+サイト名となっていること' do
    expect(page).to have_title("#{taxon.name} - BIGBAG Store")
  end

  scenario '別カテゴリーの商品名を表示しない' do
    expect(page).not_to have_content "other bag"
  end
end
