require 'rails_helper'

RSpec.feature "Products", type: :feature do
  let(:product) { create(:product, taxons: [taxon]) }
  let(:taxon) { create(:taxon) }
  let!(:related_product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 5, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  scenario '商品詳細ページが正しく表示される' do
    expect(page).to have_content product.name
    expect(page).to have_content product.description
    expect(page).to have_content product.display_price
  end

  scenario 'タイトルが商品名+サイト名となっている' do
    expect(page).to have_title("#{product.name} - BIGBAG Store")
  end

  scenario '一覧ページへ戻るをクリックしたら一覧ページへ戻れる' do
    click_on "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  scenario '関連商品が正しく表示される' do
    expect(page).to have_content related_product.name
    expect(page).to have_content related_product.display_price
    expect(page).to have_selector('img[alt="products-img"]')
  end

  scenario '関連商品を4件表示する' do
    expect(page).to have_selector ".productBox", count: 4
  end

  scenario '関連商品5件作成され、1件は表示しない' do
    expect(page).not_to have_content related_products[4].name
  end

  scenario '関連商品をクリックしたらその商品詳細ページへ飛べる' do
    click_on related_product.name
    expect(current_path).to eq potepan_product_path(related_product.id)
  end
end
