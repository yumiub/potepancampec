require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe 'full_title(page_title)' do
    context 'page_titleに値が入っている場合' do
      it 'page_titleとBASE_TITLEが表示される' do
        expect(full_title('RUBY ON RAILS TOTE')).to eq('RUBY ON RAILS TOTE - BIGBAG Store')
      end
    end

    context 'page_titleが空白の場合' do
      it 'BASE_TITLEのみ表示される' do
        expect(full_title('')).to eq('BIGBAG Store')
      end
    end

    context 'page_titleがnilの場合' do
      it 'BASE_TITLEのみ表示される' do
        expect(full_title(nil)).to eq('BIGBAG Store')
      end
    end
  end
end
