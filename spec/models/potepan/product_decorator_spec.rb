require 'rails_helper'

RSpec.describe Potepan::ProductDecorator, type: :model do
  describe 'related_products' do
    let(:taxon) { create(:taxon) }
    let(:taxon2) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon, taxon2]) }
    let(:related_products) { create_list(:product, 4, taxons: [taxon, taxon2]) }
    let(:unrelated_taxon) { create(:taxon) }
    let(:unrelated_product) { create(:product, taxons: [unrelated_taxon]) }

    it '詳細商品の関連商品が表示される' do
      expect(product.related_products).to match_array related_products
    end

    it '詳細商品は関連商品の中に含まれていない' do
      expect(product.related_products).not_to include product
    end

    it '関連商品同士の重複がない' do
      expect(product.related_products).to eq product.related_products.uniq
    end

    it '関連しない商品が含まれていない' do
      expect(product.related_products).not_to include unrelated_product
    end
  end
end
