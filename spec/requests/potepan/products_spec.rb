require 'rails_helper'

RSpec.describe "PotepanProducts", type: :request do
  describe 'GET #show' do
    let(:taxon) { create(:taxon) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_product) { create(:product, taxons: [taxon]) }
    let(:image) { create(:image) }

    before do
      related_product.images << image
      get potepan_product_path(product.id)
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(200)
    end

    it '期待されているproduct情報が返ってくること' do
      expect(response.body).to include product.name
      expect(response.body).to include product.display_price.to_s
      expect(response.body).to include product.description
    end

    it '期待されている関連商品情報が返ってくる' do
      expect(response.body).to include related_product.name
      expect(response.body).to include related_product.display_price.to_s
      expect(response.body).to include related_product.display_image.attachment(:large)
    end
  end
end
